# OpenPGP lint

Sample output:

```
Primary key: 653909A2F0E37C106F5FAF546C8857E0D8E8F074
User ID: "Wiktor Kwapisiewicz <wiktor@metacode.biz>"
    Creat: 2019-07-14 19:44:06 UTC
    Expir: 2021-01-01 12:00:01 UTC
    Flags: C
    Hash : SHA512
    PK Al: RSAEncryptSign RSA: 4096
    PHash: [SHA512, SHA384, SHA256, SHA224]
    PSymm: [AES256, AES192, AES128, Camellia256, Camellia192, Camellia128, Twofish]

Key: 653909A2F0E37C106F5FAF546C8857E0D8E8F074
  PK Algo: RSAEncryptSign Some(4096)
  Version: 4
Key: EF1EE0FA9420F804FDEFC02697FDEF34DAB8F82B
  PK Algo: RSAEncryptSign Some(2048)
  Version: 4
    Creat: 2019-07-14 19:46:17 UTC
    Expir: 2021-01-01 12:00:02 UTC
    Flags: S
    Hash : SHA512
    PK Al: RSAEncryptSign RSA: 4096
  Emb: PrimaryKeyBinding
    Hash : SHA512
    PK Al: RSAEncryptSign RSA: 2048

Key: F9FE4648F5F4A9CE23BA298860D2F50529E2DE4F
  PK Algo: RSAEncryptSign Some(2048)
  Version: 4
    Creat: 2019-07-14 19:46:17 UTC
    Expir: 2021-01-01 12:00:02 UTC
    Flags: EtEr
    Hash : SHA512
    PK Al: RSAEncryptSign RSA: 4095

Key: DD977564E03AC260B414A1A53B6DFCC964CFEBC4
  PK Algo: RSAEncryptSign Some(2048)
  Version: 4
    Creat: 2019-07-14 19:46:17 UTC
    Expir: 2021-01-01 12:00:02 UTC
    Flags: A
    Hash : SHA512
    PK Al: RSAEncryptSign RSA: 4096

Key: 9C05936329761301B055BBBBB9BEAFCADA89714C
  PK Algo: RSAEncryptSign Some(4096)
  Version: 4
    Creat: 2017-01-01 19:25:58 UTC
    Expir: 2018-01-01 19:25:58 UTC
    Flags: A
    Hash : SHA512
    PK Al: RSAEncryptSign RSA: 4096

  Rev: 9C05936329761301B055BBBBB9BEAFCADA89714C
      Creat: 2017-10-18 14:07:45 UTC
      Reasn: No reason specified: Revoke Infineon RSA generated keys

Key: 4176054F590205856C36490AE88D0649795CAC0B
  PK Algo: RSAEncryptSign Some(4096)
  Version: 4
    Creat: 2017-01-01 19:24:39 UTC
    Expir: 2018-01-01 19:24:39 UTC
    Flags: S
    Hash : SHA512
    PK Al: RSAEncryptSign RSA: 4093
  Emb: PrimaryKeyBinding
    Hash : SHA512
    PK Al: RSAEncryptSign RSA: 4093

  Rev: 4176054F590205856C36490AE88D0649795CAC0B
      Creat: 2017-10-18 14:07:44 UTC
      Reasn: No reason specified: Revoke Infineon RSA generated keys

Key: C200C506497B07C07A41AC31D27076C34BC6B8C6
  PK Algo: RSAEncryptSign Some(4096)
  Version: 4
    Creat: 2017-01-01 19:20:34 UTC
    Expir: 2018-01-01 19:20:34 UTC
    Flags: A
    Hash : SHA512
    PK Al: RSAEncryptSign RSA: 4096

  Rev: C200C506497B07C07A41AC31D27076C34BC6B8C6
      Creat: 2017-10-18 14:07:44 UTC
      Reasn: No reason specified: Revoke Infineon RSA generated keys

Key: DB7D5F272EAD8CE85490BAE2F5DAB3B2F1143313
  PK Algo: RSAEncryptSign Some(4096)
  Version: 4
    Creat: 2017-01-01 19:19:25 UTC
    Expir: 2018-01-01 19:19:25 UTC
    Flags: S
    Hash : SHA512
    PK Al: RSAEncryptSign RSA: 4096
  Emb: PrimaryKeyBinding
    Hash : SHA512
    PK Al: RSAEncryptSign RSA: 4095

  Rev: DB7D5F272EAD8CE85490BAE2F5DAB3B2F1143313
      Creat: 2017-10-18 14:07:44 UTC
      Reasn: No reason specified: Revoke Infineon RSA generated keys

Key: 9C4C6BB520FC2CE3AD8AA552AB77771ACFC6E4E6
  PK Algo: RSAEncryptSign Some(4096)
  Version: 4
    Creat: 2017-01-01 19:13:44 UTC
    Expir: 2018-01-01 19:13:44 UTC
    Flags: A
    Hash : SHA512
    PK Al: RSAEncryptSign RSA: 4094

  Rev: 9C4C6BB520FC2CE3AD8AA552AB77771ACFC6E4E6
      Creat: 2017-10-18 14:07:43 UTC
      Reasn: No reason specified: Revoke Infineon RSA generated keys

Key: 9FB340AED954473E51683D156A304C622D907259
  PK Algo: RSAEncryptSign Some(4096)
  Version: 4
    Creat: 2017-01-01 19:10:28 UTC
    Expir: 2018-01-01 19:10:28 UTC
    Flags: S
    Hash : SHA512
    PK Al: RSAEncryptSign RSA: 4095
  Emb: PrimaryKeyBinding
    Hash : SHA512
    PK Al: RSAEncryptSign RSA: 4096

  Rev: 9FB340AED954473E51683D156A304C622D907259
      Creat: 2017-10-18 14:07:26 UTC
      Reasn: No reason specified: Revoke Infineon RSA generated keys

Key: 42CCBA7DCD0E3A818425CCAB7802C0AFF93B9048
  PK Algo: RSAEncryptSign Some(4096)
  Version: 4
    Creat: 2017-12-11 13:12:52 UTC
    Expir: 2018-12-31 00:00:00 UTC
    Flags: EtEr
    Hash : SHA512
    PK Al: RSAEncryptSign RSA: 4094

Key: D92D260EDA105F50057FA137413900012F8DC928
  PK Algo: RSAEncryptSign Some(4096)
  Version: 4
    Creat: 2017-12-11 13:13:16 UTC
    Expir: 2018-12-31 00:00:00 UTC
    Flags: A
    Hash : SHA512
    PK Al: RSAEncryptSign RSA: 4096

Key: 59A29DEA8D37388C656863DFB97A1EE09DB417EC
  PK Algo: RSAEncryptSign Some(4096)
  Version: 4
    Creat: 2019-07-14 19:46:17 UTC
    Expir: 2021-01-01 12:00:02 UTC
    Flags: S
    Hash : SHA512
    PK Al: RSAEncryptSign RSA: 4096
  Emb: PrimaryKeyBinding
    Hash : SHA512
    PK Al: RSAEncryptSign RSA: 4093
```

