use openpgp::parse::Parse;
use sequoia_openpgp as openpgp;
use std::time::SystemTime;
use chrono::offset::Utc; use chrono::DateTime;

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let parser = openpgp::cert::CertParser::from_reader(std::io::stdin())?; //_file("/home/wiktor/src/pep/sqv-gaps-key.txt")?;

    let to_utc = |sys: Option<SystemTime>| sys.map(|sys| Into::<DateTime<Utc>>::into(sys)).map(|date| date.to_string()).unwrap_or("".to_string());

    for cert in parser {
        if let Ok(cert) = cert {
            //let cert = cert.with_policy(p, SystemTime::now())?;
            println!("Primary key: {:X}", cert.primary_key().fingerprint());
            for part in cert.userids() {
                println!("User ID: {:?}", part.to_string());
                for sig in part.self_signatures().iter().take(1) {
                    if let openpgp::packet::Signature::V4(sig) = sig {
                        println!("    Creat: {}", to_utc(sig.signature_creation_time()));
                        println!("    Expir: {}", to_utc(sig.key_expiration_time(&cert.primary_key())));
                        if let Some(flags) = sig.key_flags() {
                            println!("    Flags: {:?}", flags);
                        }
                        println!("    Hash : {:?}", sig.hash_algo());
                        println!("    PK Al: {:?} {}", sig.pk_algo(), match sig.mpis() {
                            openpgp::crypto::mpi::Signature::RSA { s } => format!("RSA: {}", s.bits()),
                            _ => "".to_string()
                        });
                        if let Some(aead) = sig.preferred_aead_algorithms() {
                            println!("    AEAD : {:?}", aead);
                        }
                        if let Some(hashes) = sig.preferred_hash_algorithms() {
                            println!("    PHash: {:?}", hashes);
                        }
                        if let Some(cmp) = sig.preferred_compression_algorithms() {
                            println!("    PCmpr: {:?}", cmp);
                        }
                        if let Some(ks) = sig.preferred_key_server() {
                            println!("    PKsrv: {:?}", ks);
                        }
                        if let Some(sym) = sig.preferred_symmetric_algorithms() {
                            println!("    PSymm: {:?}", sym);
                        }
                        println!();
                    }
                }
            }

            for key in cert.keys() {
                println!("Key: {:X}", key.fingerprint());
                println!("  PK Algo: {:?} {:?}", key.pk_algo(), key.mpis().bits());
                println!("  Version: {:?}", key.version());
                for sig in key.self_signatures().iter().take(1) {
                    println!("    Creat: {}", to_utc(sig.signature_creation_time()));
                    println!("    Expir: {}", to_utc(sig.key_expiration_time(&key)));
                    if let Some(flags) = sig.key_flags() {
                        println!("    Flags: {:?}", flags);
                    }
                    println!("    Hash : {:?}", sig.hash_algo());
                    println!("    PK Al: {:?} {}", sig.pk_algo(), match sig.mpis() {
                        openpgp::crypto::mpi::Signature::RSA { s } => format!("RSA: {}", s.bits()),
                        _ => "".to_string()
                    });
                    for emb in sig.embedded_signatures().take(1) {
                        println!("  Emb: {:?}", emb.typ());
                        println!("    Hash : {:?}", emb.hash_algo());
                        println!("    PK Al: {:?} {}", emb.pk_algo(), match emb.mpis() {
                            openpgp::crypto::mpi::Signature::RSA { s } => format!("RSA: {}", s.bits()),
                            _ => "".to_string()
                        });
                    }
                    println!();
                }

                for rev in key.self_revocations().iter().take(1) {
                    println!("  Rev: {:X}", key.fingerprint());
                    println!("      Creat: {}", to_utc(rev.signature_creation_time()));
                    if let Some(reason) = rev.reason_for_revocation() {
                        println!("      Reasn: {}: {}", reason.0, String::from_utf8_lossy(reason.1));
                    }
                    println!();
                }
            }
        }
    }

    Ok(())
}
